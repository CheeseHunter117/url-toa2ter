FROM docker.io/messense/cargo-zigbuild as builder

# Get source code into container
RUN mkdir -p /sourceCode/src
COPY Cargo.toml Cargo.lock /sourceCode/
COPY src/ /sourceCode/src/
WORKDIR /sourceCode/

# Actually build the program
RUN mkdir -p /out/
RUN cargo zigbuild --release --target=x86_64-unknown-linux-gnu --target-dir=/out/

#####

FROM debian:stable-slim AS runtime

COPY --from=builder /out/x86_64-unknown-linux-gnu/release/TOA2TER /usr/bin/

# Bot uses HTTPS REST API to communicate with Discord
EXPOSE 443

CMD ["/usr/bin/TOA2TER"]

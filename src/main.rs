#![allow(non_snake_case)]

use std::env;

use serenity::all::EditMessage;
use serenity::async_trait;
use serenity::model::{channel::Message, gateway::Ready};
use serenity::prelude::*;

use url::Url;

use url_handler::{domain::Domain, get_query_value};
use url_handler::{tiktok, twitter, youtube};

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    // Set a handler to be called on the `ready` event. This is called when a
    // shard is booted, and a READY payload is sent by Discord. This payload
    // contains data like the current user's guild Ids, current user data,
    // private channels, and more.
    //
    // In this case, just print what the current user's username is.
    async fn ready(&self, _: Context, ready: Ready) {
        tracing::info!("{} is connected!", ready.user.name);
    }

    // Set a handler for the `message` event - so that whenever a new message
    // is received - the closure (or function) passed will be called.
    //
    // Event handlers are dispatched through a threadpool, and so multiple
    // events can be dispatched simultaneously.
    async fn message(&self, ctx: Context, msg: Message) {
        // Avoid endless loops by replying to yourself
        if msg.author.id == ctx.cache.current_user().id {
            return;
        }

        // Split the message into their whitespace separated parts to look for URLs
        let message_parts = msg.content.split_whitespace().map(String::from);

        // Look at all the parts and run the cleaning function on those that start with "http"
        let clean_urls: Vec<_> = message_parts
            .filter(|p| p.starts_with("http"))
            .filter_map(|part| handle_possible_url(&part).map(|clean| (part, clean)))
            // Make sure that the bot won't reply with the identical URL that was posted
            .filter(|(part, (clean, _))| part != clean)
            .collect();

        // If the message did not create any clean URLs, avoid trying to reply
        if clean_urls.is_empty() {
            return;
        }

        let youtube_response = clean_urls
            .iter()
            .filter(|(_, (_, domain))| domain == &Domain::YouTube)
            .map(|(_, (clean, _))| clean.clone())
            .collect::<Vec<_>>()
            .join("\n");

        let rest_response = clean_urls
            .iter()
            .filter(|(_, (_, domain))| domain != &Domain::YouTube)
            .map(|(_, (clean, _))| clean.clone())
            .collect::<Vec<_>>()
            .join("\n");

        // Sending a message can fail, due to a network error, an
        // authentication error, or lack of permissions to post in the
        // channel, so log to stdout when some error happens, with a
        // description of it.

        // YouTube first, as I want to suppress the embeds on that message.
        if !youtube_response.is_empty() {
            match msg.reply(&ctx, youtube_response).await {
                Ok(mut sent_msg) => {
                    // TODO: implement waiting for Discord to actually load embeds like it is explained in the Serenity
                    //       documentation under `suppress_embeds`
                    if let Err(why) = sent_msg
                        .edit(&ctx, EditMessage::new().suppress_embeds(true))
                        .await
                    {
                        tracing::warn!("Failed suppressing embeds on YouTube response: {:?}", why);
                    } else {
                        tracing::info!(
                            "YouTube Response: Successfully replied to (name: {:?}, msg_id: {:?})",
                            msg.author.name,
                            msg.id
                        );
                    }
                }
                Err(why) => tracing::warn!("Error replying to message: {:?}", why),
            }
        }

        // And now the second message with the rest of the cleaned URLs which should not have embeds suppressed.
        if !rest_response.is_empty() {
            if let Err(why) = msg.reply(&ctx, rest_response).await {
                tracing::warn!("Error replying to message: {:?}", why);
            } else {
                tracing::info!(
                    "Rest Response: Successfully replied to (name: {:?}, msg_id: {:?})",
                    msg.author.name,
                    msg.id
                );
            }
        }
    }
}

#[tracing::instrument]
fn handle_possible_url(url: &str) -> Option<(String, Domain)> {
    let domain = url
        .parse::<Domain>()
        .map_err(|err| tracing::warn!("Failed to parse {:?}: {:?}", url, err))
        .ok()?;

    match domain {
        Domain::TikTok => tiktok::get_download_url(url, tiktok::Watermark::default())
            .map_err(|err| tracing::warn!("Failed to get ProxiTok download URL: {:?}", err))
            .ok(),

        Domain::YouTube => {
            let url_object = Url::parse(url).expect("Parse input URL to URL object");

            // Only act on the URL if it contains tracking data.
            // Otherwise the bot would trigger too often.
            if get_query_value(&url_object, "si").is_some() {
                youtube::clean_video_url(url)
                    .map_err(|err| {
                        tracing::warn!("Failed to clean YouTube URL: {:?}", err);
                    })
                    .map(|(_, short)| short)
                    .ok()
            } else {
                None
            }
        }

        Domain::Twitter => twitter::construct_fxtwitter_url(url)
            .map_err(|err| tracing::warn!("Failed to construct FxTwitter URL: {:?}", err))
            .ok(),

        Domain::FxTwitter => twitter::remove_queries(url)
            .map_err(|err| tracing::warn!("Failed to clean FxTwitter URL: {:?}", err))
            .ok(),

        Domain::Other => None,

        _ => unreachable!(),
    }
    .map(|clean_url| (String::from(clean_url), domain))
}

#[tokio::main]
async fn main() -> Result<(), String> {
    let tracing_sub = tracing_subscriber::FmtSubscriber::new();
    tracing::subscriber::set_global_default(tracing_sub)
        .expect("Set global default tracing subscriber");

    // Basic CLI argument parsing
    let args: Vec<_> = std::env::args().collect();

    if args.contains(&"-h".to_string()) || args.contains(&"--help".to_string()) {
        println!("Program: TOA2TER");
        println!(
            "Authors: {authors}",
            authors = env!("CARGO_PKG_AUTHORS")
                .split(':')
                .collect::<Vec<_>>()
                .join(", ")
        );
        println!("Version: {version}", version = env!("CARGO_PKG_VERSION"));
        println!("Repository: {repo}", repo = env!("CARGO_PKG_REPOSITORY"));
        println!();
        println!("The program expects the environment variable DISCORD_BOT_TOKEN to be set");

        return Ok(());
    }

    if args.contains(&"-V".to_string())
        || args.contains(&"-v".to_string())
        || args.contains(&"--version".to_string())
    {
        println!("{version}", version = env!("CARGO_PKG_VERSION"));
        return Ok(());
    }

    // Configure the client with your Discord bot token in the environment.
    let token = env::var("DISCORD_BOT_TOKEN").expect("DISCORD_BOT_TOKEN environment variable");
    // Set gateway intents, which decides what events the bot will be notified about
    let intents = GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::DIRECT_MESSAGES
        | GatewayIntents::MESSAGE_CONTENT;

    // Create a new instance of the Client, logging in as a bot. This will
    // automatically prepend your bot token with "Bot ", which is a requirement
    // by Discord for bot users.
    let mut client = Client::builder(&token, intents)
        .event_handler(Handler)
        .await
        .expect("Creating client");

    // Finally, start a single shard, and start listening to events.
    //
    // Shards will automatically attempt to reconnect, and will perform
    // exponential backoff until it reconnects.
    if let Err(why) = client.start().await {
        tracing::error!("Client error: {:?}", why);
        return Err("Could not connect bot to Discord".to_string());
    }

    Ok(())
}

# About
A Discord bot written in Rust using the [`serenity` framework](https://github.com/serenity-rs/serenity).

It replies to messages that include Twitter, TikTok, and YouTube URLs.
For Twitter and YouTube, the URLs will simply be cleaned up and have tracking queries removed.

For TikTok, the bot will find a [**ProxiTok**](https://github.com/pablouser1/ProxiTok) URL that points to the same video content but avoids having to follow TikTok's tracking.
The video should also embed easily into Discord's chat window.

# Usage
Compile the executable using `cargo`.
```sh
cargo build --release
```
or
```sh
cargo build --profile=size
```

After that make sure you have an environment variable `DISCORD_BOT_TOKEN` set which holds your bot's token that you get from the [Discord Developer Page](https://discord.com/developers/).

Also make sure your bot has the _Message Content Intent_ enabled inside the developer settings.
This is needed for it to reply to users.

Last but not least, run your compiled executable.

For example:
```sh
./target/release/TOA2TER | tee log.txt
```

For all other things like adding your bot to a server, please read [Discord's documentation](https://discord.com/developers/docs/intro).